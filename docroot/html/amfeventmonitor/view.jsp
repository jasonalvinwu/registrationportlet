<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.liferay.portal.kernel.exception.SystemException" %>
<%@ page import="com.amf.registration.model.UserEvent" %>
<%@ page import="com.liferay.portal.model.User" %>
<%@ page import="com.amf.registration.service.UserEventLocalServiceUtil" %>
<%@ page import="com.amf.registration.service.UserEventServiceUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@page import="javax.portlet.RenderRequest"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<liferay-theme:defineObjects />
<portlet:defineObjects />

<liferay-ui:tabs names="All,Registration,Login" refresh="false" tabsValues= "All,Registration,Login">
	
	<liferay-ui:section>
		<%
			User currentUser = themeDisplay.getUser();
			long userGroupId;
			long currentUserId;
			currentUserId = currentUser.getUserId();
			// Pass a dummy groupId because groupId does not exist for guest user
			if (currentUser.isDefaultUser()) {
				userGroupId = 0;
			} else {
				userGroupId = currentUser.getGroupId();	
			}
	    	int count = UserEventServiceUtil.getEventsCount(currentUserId, userGroupId);        
	    	int delta = 20;        
	    	Integer cur = (Integer)request.getAttribute("cur");        
	    	if(cur == null){
	        	cur = 1;
	    	}
	    	PortletURL portletURL = renderResponse.createRenderURL();
	    	portletURL.setParameter("jspPage", "/html/amfeventmonitor/view.jsp");
		%>
		<liferay-ui:search-container curParam="all" iteratorURL="<%= portletURL %>" delta="<%= delta %>" emptyResultsMessage="No logins/registrations Found." total="<%= count %>">
	
	    	<liferay-ui:search-container-results results="<%=UserEventServiceUtil.getUserEvents(currentUserId, userGroupId, searchContainer.getStart(), searchContainer.getEnd()) %>" />
	    	<liferay-ui:search-container-row modelVar="events" className="com.amf.registration.model.UserEvent" >
	        	<liferay-ui:search-container-column-text name="Date" value="<%=events.getDate().toString() %>" />
		        <liferay-ui:search-container-column-text name="Username" value="<%=events.getScreenName() %>" />
		        <liferay-ui:search-container-column-text name="User ID" value="<%=Long.toString(events.getUserId()) %>" />
		        <liferay-ui:search-container-column-text name="IP Address" value="<%=events.getIpAddress() %>" />
		        <liferay-ui:search-container-column-text name="Event Type" value="<%=events.getType() %>" />
	    	</liferay-ui:search-container-row>
	    	<%        
	    		portletURL.setParameter("cur", String.valueOf(cur));        
	    		searchContainer.setIteratorURL(portletURL);    
	    	%>
	    	<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" />
		</liferay-ui:search-container>	
	</liferay-ui:section>
 	
	<liferay-ui:section>
		<%
			String registration = "registration";
			User currentUser = themeDisplay.getUser();
			long currentUserId = currentUser.getUserId();
			long userGroupId;
			// Pass a dummy groupId because groupId does not exist for guest user
			if (currentUser.isDefaultUser()) {
				userGroupId = 0;	
			} else {
				userGroupId = currentUser.getGroupId();
			}
		    int count = UserEventServiceUtil.getEventsCountByType(currentUserId, userGroupId, "registration");        
		    int delta = 20;        
		    Integer cur = (Integer)request.getAttribute("cur");        
		    if(cur == null){
		        cur = 1;
		    }
		    PortletURL portletURL = renderResponse.createRenderURL();
		    portletURL.setParameter("jspPage", "/html/amfeventmonitor/view.jsp");
		%>

		<liferay-ui:search-container curParam="registration" iteratorURL="<%= portletURL %>" delta="<%= delta %>" emptyResultsMessage="No logins/registrations Found." total="<%= count %>">

    		<liferay-ui:search-container-results results="<%=UserEventServiceUtil.findEventByType(currentUserId, userGroupId, registration, searchContainer.getStart(), searchContainer.getEnd()) %>" />
    		<liferay-ui:search-container-row modelVar="events" className="com.amf.registration.model.UserEvent" >
		        <liferay-ui:search-container-column-text name="Date" value="<%=events.getDate().toString() %>" />
		        <liferay-ui:search-container-column-text name="Username" value="<%=events.getScreenName() %>" />
		        <liferay-ui:search-container-column-text name="User ID" value="<%=Long.toString(events.getUserId()) %>" />
		        <liferay-ui:search-container-column-text name="IP Address" value="<%=events.getIpAddress() %>" />
		        <liferay-ui:search-container-column-text name="Event Type" value="<%=events.getType() %>" />
    		</liferay-ui:search-container-row>
    		<%
    			portletURL.setParameter("cur", String.valueOf(cur));        
    		  	searchContainer.setIteratorURL(portletURL);    
    		 %>
   			<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" />
		</liferay-ui:search-container>
	</liferay-ui:section>
	
	<liferay-ui:section>
		<%
			String login = "login";
			User currentUser = themeDisplay.getUser();
			long currentUserId = currentUser.getUserId();
			long userGroupId;
			if (currentUser.isDefaultUser()) {
				userGroupId = 0;
			} else {
				userGroupId = currentUser.getGroupId();	
			}
		    int count = UserEventServiceUtil.getEventsCountByType(currentUserId, userGroupId, "login");        
		    int delta = 20;        
		    Integer cur1 = (Integer)request.getAttribute("cur");        
		    if(cur1 == null){
		        cur1 = 1;
		    }
		    PortletURL portletURLLogin = renderResponse.createRenderURL();
		    portletURLLogin.setParameter("jspPage", "/html/amfeventmonitor/view.jsp?page=login");
		%>

		<liferay-ui:search-container curParam="login" iteratorURL="<%= portletURLLogin %>" delta="<%= delta %>" emptyResultsMessage="No logins/registrations Found." total="<%= count %>">

    		<liferay-ui:search-container-results results="<%=UserEventServiceUtil.findEventByType(currentUserId, userGroupId, login, searchContainer.getStart(), searchContainer.getEnd()) %>" />
    		<liferay-ui:search-container-row modelVar="events" className="com.amf.registration.model.UserEvent" >
		        <liferay-ui:search-container-column-text name="Date" value="<%=events.getDate().toString() %>" />
		        <liferay-ui:search-container-column-text name="Username" value="<%=events.getScreenName() %>" />
		        <liferay-ui:search-container-column-text name="User ID" value="<%=Long.toString(events.getUserId()) %>" />
		        <liferay-ui:search-container-column-text name="IP Address" value="<%=events.getIpAddress() %>" />
		        <liferay-ui:search-container-column-text name="Event Type" value="<%=events.getType() %>" />
    		</liferay-ui:search-container-row>
    		<%
    			portletURLLogin.setParameter("cur", String.valueOf(cur1));        
    		  	searchContainer.setIteratorURL(portletURLLogin);    
    		%>
    		<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" />
		</liferay-ui:search-container>
	</liferay-ui:section>
	
</liferay-ui:tabs>

