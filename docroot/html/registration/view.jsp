<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<portlet:actionURL name="validateUser" var="validateUserURL" />

<%
boolean isSignedIn = themeDisplay.isSignedIn();
if (isSignedIn) {
	%>
	You are already signed in!
	<%
	}
else {
%>

<aui:form name="myForm" id="myForm" method="POST" action="<%=validateUserURL.toString() %>">

	<div class="form-group">
		<div class="controls">
			<aui:input name="first_name" id="first_name" type="text" label="First Name:" >
				<aui:validator name="required" errorMessage="Please enter a first name." />
				<aui:validator name="alphanum" errorMessage="Only alphanumeric characters are allowed." />
				<aui:validator name="maxLength" errorMessage="Only 50 characters are allowed." >50</aui:validator>
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="last_name" id="last_name" type="text" label="Last Name:" >
				<aui:validator name="required" errorMessage="Please enter a last name." />
				<aui:validator name="alphanum" errorMessage="Only alphanumeric characters are allowed." />
				<aui:validator name="maxLength" errorMessage="Only 50 characters are allowed." >50</aui:validator>
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="email_address" id="email_address" class="form-control field-required" type="text" label="Email address:" >
				<aui:validator name="required" errorMessage="Please enter an e-mail address." />
				<aui:validator name="email" />
				<aui:validator name="maxLength" errorMessage="Only 255 characters are allowed." >255</aui:validator>
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="username" id="username" class="form-control field-required" type="text" label="Username:">
				<aui:validator name="required" errorMessage="Please enter a username." />
				<aui:validator name="alphanum" errorMessage="Only alphanumeric characters are allowed." />
				<aui:validator name="minLength" errorMessage="Username must contain at least 4 characters." >4</aui:validator>
				<aui:validator name="maxLength" errorMessage="Username must contain no more than 16 characters.">16</aui:validator>
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<aui:select name="male" class="controls" label="Gender:" required="true">
			<aui:option value="true">Male</aui:option>
			<aui:option value="false">Female</aui:option>
		</aui:select>
	</div>
	
	<div class="form-group">
		<aui:select name="b_month" class="controls" label="Birth month:" required="true">
			<option value="1">January</option>
			<option value="2">February</option>
			<option value="3">March</option>
			<option value="4">April</option>
			<option value="5">May</option>
			<option value="6">June</option>
			<option value="7">July</option>
			<option value="8">August</option>
			<option value="9">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
		</aui:select>
		
		<div class="controls">
			<aui:input name="b_day" id="b_day" class="form-control field-required" type="number" label="Birth day:" >
				<aui:validator name="required" errorMessage="Please enter a username." />
				<aui:validator name="digits" errorMessage="Birth date must be a number." />
				<aui:validator name="range" errorMessage="Birth date must be a valid date.">[1,31]</aui:validator>
			</aui:input>
		</div>
		
		<div class="controls">
			<aui:input name="b_year" id="b_year" class="form-control field-required" type="number" label="Birth year:" >
				<aui:validator name="required" errorMessage="Please enter a username." />
				<aui:validator name="digits" errorMessage="Birth year must be a number." />
				<aui:validator name="range" errorMessage="Birth year must be a valid living year.">[1900,2015]</aui:validator>
			</aui:input>
		</div>
	</div>
	
	<div class="controls">
		<aui:input name="password1" id="password1" class="form-control field-required" type="password" label="Enter password:" >
			<aui:validator name="required" errorMessage="Please enter a password." />
			<aui:validator name="minLength" errorMessage="Password must be a minimum of 6 characters.">6</aui:validator>
		</aui:input>
	</div>
		
	<div class="controls">
		<aui:input name="password2" id="password2" class="form-control field-required" type="password" label="Re-enter password:" >
			<aui:validator name="required" errorMessage="Please re-enter the password." />
			<aui:validator name="minLength" errorMessage="Password must be a minimum of 6 characters.">6</aui:validator>
			<aui:validator name="equalTo" errorMessage="Passwords must match.">'#<portlet:namespace />password1'</aui:validator>
		</aui:input>
	</div>
		
	<div class="form-group">
		<div class="controls">
			<aui:input name="home_phone" id="home_phone" type="number" label="Home Phone:" >
				<aui:validator name="rangeLength" errorMessage="Number must be 10 digits long.">[10,10]</aui:validator>
				<aui:validator name="digit" errorMessage="Phone number must be digits only." />
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="mobile_phone" id="mobile_phone" type="number" label="Mobile Phone: " >
				<aui:validator name="rangeLength" errorMessage="Number must be 10 digits long.">[10,10]</aui:validator>
				<aui:validator name="digit" errorMessage="Phone number must be digits only." />
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="address" id="address" type="text" label="Address 1:">
				<aui:validator name="maxLength" errorMessage="Address cannot exceed 255 characters">255</aui:validator>
				<aui:validator name="required" errorMessage="Please enter an address." />
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="address2" id="address2" type="text" label="Address 2:">
				<aui:validator name="alphanum" errorMessage="Address can only contain alphanumeric characters." />
				<aui:validator name="maxLength" errorMessage="Address cannot exceed 255 characters">255</aui:validator>
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="city" id="city" type="text" label="City:">
				<aui:validator name="alphanumeric" errorMessage="City can only contain alphanumeric characters." />
				<aui:validator name="maxLength" errorMessage="City cannot exceed 255 characters">255</aui:validator>
				<aui:validator name="required" errorMessage="Please enter a city." />
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="state" id="state" type="text" label="State:">
				<aui:validator name="required" errorMessage="Please enter a state." />
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="zip" id="zip" type="number" label="Zip Code:">
				<aui:validator name="required" errorMessage="Please enter a zip code." />
				<aui:validator name="rangeLength" errorMessage="Must be 5 digits.">[5, 5]</aui:validator>
				<aui:validator name="digit" errorMessage="Zip code must be a number."></aui:validator>
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:select name="security_question" id="security_question" label="Security Question:" required="true">
				<option selected disabled>Must choose one of the following</option>
				<option value="What is your mother's maiden name?">What is your mother's maiden name?</option>
				<option value="What is the make of your first car?">What is the make of your first car?</option>
				<option value="What is the make of your first car?">What is your high school mascot?</option>
				<option value="What is your favorite actor?">What is your favorite actor?</option>
			</aui:select>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:input name="security_answer" id="security_answer" type="text" label="Answer:">
				<aui:validator name="alphanum" errorMessage="Answer must contain only alphanumeric characters." />
				<aui:validator name="maxLength" errorMessage="Answer cannot exceed 255 characters">255</aui:validator>
			</aui:input>
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			I have read, understand, and agree with the Terms of Use governing my access to and use of the Acme Movie Fanatics web site.
			<aui:input name="accepted_tou" id="accepted_tou" type="checkbox" label="Terms of Use:" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="controls">
			<aui:button type="button" onClick="validate()" cssClass="aui-button" value="Submit" />
		</div>
	</div>	
	
		
</aui:form>

<%
}
%>