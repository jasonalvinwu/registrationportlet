validate = function() {
	
	var email_address = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_email_address.value;
	var b_month = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_b_month.value;
	var b_day = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_b_day.value;
	var b_year = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_b_year.value;
	var password1 = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_password1.value;
	var password2 = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_password2.value;
	
	var home_phone = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_home_phone.value;
	var mobile_phone = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_mobile_phone.value;

	var address = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_address.value;
	var city = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_city.value;
	var state = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_state.value;
	
	var accepted_tou = document._registrationportlet_WAR_registrationportlet_myForm._registrationportlet_WAR_registrationportlet_accepted_tou.value;
	
	var stateValid;
	var streetValid;
	var emailValid;
	var passValid;
	var dateValid;
	
	// Validates for date integrity
	if (isDate(b_month, b_day, b_year)) {
		dateValid = true;
	} else {
		dateValid = false;
	}
	
	// Validates for e-mail address syntax
	var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if (!pattern.test(email_address)) {
		alert("E-mail address is not valid!");
	} else {
		emailValid = true;
	}
	
	// Validates for password syntax and that both passwords match
	var passpattern = /^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{6,}$/;
	if (!passpattern.test(password1)) {
		alert("Password must contain at least 6 characters, one uppercase letter, one number, and one special character.");
	} else {
		passValid = true;
	}
	
	// Validates for address syntax
	var streetPattern = /^[a-z\d\-_\s]+$/i;
	if (!(streetPattern.test(address) && streetPattern.test(city))) {
		alert("Address is not valid!");
	} else {
		streetValid = true;
	}
	
	if (state != "CA") {
		alert("State must be CA!");
	}
	else {
		stateValid = true;
	}
	
	// Prevents form submission if TOU is not selected
	if (accepted_tou == "false") {
		alert("In order to register, you must accept the terms of agreement.");
	}
	else {
		// If validation checks out, proceed with form submission
		if (stateValid && streetValid && emailValid && passValid && dateValid) {
			document._registrationportlet_WAR_registrationportlet_myForm.submit();
		}
	}
	
};

// Function to determine given date is valid
isDate = function(month, date, year) {
	month = month;
	var myDate = month + "/" + date + "/" + year; 
	if (date < 1 || date > 31) {
		alert("Date is incorrect.");
		return false;
	}
	// Check for months with 31 days
	if ((month == 4 || month == 6 || month == 9 || month ==11) && date == 31) {
		alert("Date cannot be 31 for the provided month.");
		return false;
	}
	// Check for February
	if (month == 2) {
		var isLeap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (date > 29 || (date==29 && !isLeap)) {
			alert("February does not have " + date + " days!");
			return false;
		}
	}
	return true;
};