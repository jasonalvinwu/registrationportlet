/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.model.impl;

import com.amf.registration.model.UserEvent;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserEvent in entity cache.
 *
 * @author Liferay
 * @see UserEvent
 * @generated
 */
public class UserEventCacheModel implements CacheModel<UserEvent>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{index=");
		sb.append(index);
		sb.append(", screenName=");
		sb.append(screenName);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", date=");
		sb.append(date);
		sb.append(", ipAddress=");
		sb.append(ipAddress);
		sb.append(", type=");
		sb.append(type);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserEvent toEntityModel() {
		UserEventImpl userEventImpl = new UserEventImpl();

		userEventImpl.setIndex(index);

		if (screenName == null) {
			userEventImpl.setScreenName(StringPool.BLANK);
		}
		else {
			userEventImpl.setScreenName(screenName);
		}

		userEventImpl.setUserId(userId);

		if (date == Long.MIN_VALUE) {
			userEventImpl.setDate(null);
		}
		else {
			userEventImpl.setDate(new Date(date));
		}

		if (ipAddress == null) {
			userEventImpl.setIpAddress(StringPool.BLANK);
		}
		else {
			userEventImpl.setIpAddress(ipAddress);
		}

		if (type == null) {
			userEventImpl.setType(StringPool.BLANK);
		}
		else {
			userEventImpl.setType(type);
		}

		userEventImpl.resetOriginalValues();

		return userEventImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		index = objectInput.readLong();
		screenName = objectInput.readUTF();
		userId = objectInput.readLong();
		date = objectInput.readLong();
		ipAddress = objectInput.readUTF();
		type = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(index);

		if (screenName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screenName);
		}

		objectOutput.writeLong(userId);
		objectOutput.writeLong(date);

		if (ipAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ipAddress);
		}

		if (type == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(type);
		}
	}

	public long index;
	public String screenName;
	public long userId;
	public long date;
	public String ipAddress;
	public String type;
}