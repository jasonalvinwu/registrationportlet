package com.amf.registration;
import com.amf.registration.service.UserEventLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.model.User;
import com.liferay.portal.ModelListenerException;

import java.util.Date;


public class LoginModelListener extends BaseModelListener<User> {
	
	// Listens to the User model for new user creation
	public void onAfterCreate(User user) throws ModelListenerException {
		String screenName = user.getScreenName();
		long userId = user.getUserId();
		Date createDate = user.getCreateDate();
		String ipAddress = "0.0.0.0";
		String type = "registration";
		try {
			// Add user event to database after a new user is registered
			UserEventLocalServiceUtil.addEvent(screenName, userId, createDate, ipAddress, type);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
}
 