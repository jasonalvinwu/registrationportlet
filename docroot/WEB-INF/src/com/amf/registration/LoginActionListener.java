package com.amf.registration;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.amf.registration.service.UserEventLocalServiceUtil;

import java.util.Date;

import javax.servlet.http.*;

public class LoginActionListener extends Action {

	public LoginActionListener() {
		super();
	}


	public void run(HttpServletRequest request, HttpServletResponse response) {
			
			// Listener to record user event upon successful login action
			User user;
			try {
				long userId = PortalUtil.getUserId(request);
				user = UserLocalServiceUtil.getUser(userId);
				String screenName = user.getScreenName();
				String ipAddress = user.getLoginIP();
				Date date = user.getLoginDate();
				String type = "login";
				try {
					UserEventLocalServiceUtil.addEvent(screenName, userId, date, ipAddress, type);
				} catch (SystemException e) {
					e.printStackTrace();
				}
			} catch (PortalException e1) {
				e1.printStackTrace();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}

	}

}