/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service.impl;

import com.amf.registration.model.UserEvent;
import com.amf.registration.service.base.UserEventLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserEventLocalServiceImpl extends UserEventLocalServiceBaseImpl {
	
	// Adds a user event to the database when picked up by a listener
	public void addEvent(String screenName, long userId, Date date, String ipAddress, String type) throws SystemException {
		long index = counterLocalService.increment(UserEvent.class.getName());
		UserEvent ue = userEventPersistence.create(index);
		ue.setDate(date);
		ue.setUserId(userId);
		ue.setType(type);
		ue.setScreenName(screenName);
		ue.setIpAddress(ipAddress);
		userEventPersistence.update(ue);
	}
	
	// Returns a list of user events based on the type (e.g. "login", "registration")
	public List<UserEvent> findEventByType(String type, int start, int end) throws SystemException {
		return userEventPersistence.findByType(type, start, end);
	}
	
	// Returns a list of all user events
	public List<UserEvent> allEvents() throws SystemException {
		return userEventPersistence.findAll();
	}
	
	// Returns a list of user events based on the user ID
	public List<UserEvent> getUserEventsByUser(long userId, int start, int end) throws SystemException {
		List<UserEvent> allEvents = allEvents();
		List<UserEvent> allEventsByUser = new ArrayList<UserEvent>();
		for (UserEvent events : allEvents) {
			if (events.getUserId() == userId) {
				allEventsByUser.add(events);
			}
		}
		return allEventsByUser;
		
	}
	
	// Returns a list of users based on the user ID and type
	public List<UserEvent> getUserEventsByUserByType(long userId, String type, int start, int end) throws SystemException {
		List<UserEvent> eventByType = findEventByType(type, start, end);
		List<UserEvent> byUserByType = new ArrayList<UserEvent>();
		for (UserEvent events : eventByType) {
			if (events.getUserId() == userId) {
				byUserByType.add(events);
			}
		}
		return byUserByType;
	}
	
	// Returns a count of all the user events
	public int getEventsCount() throws SystemException {
		return userEventPersistence.countAll();
	}
	
	// Returns a count of all events by the current user
	public int getEventsCountByUser(long userId) throws SystemException {
		return userEventPersistence.countByUserID(userId);
	}
	
	// Returns a count of all events by the type
	public int getEventsCountByType(String type) throws SystemException {
		return userEventPersistence.countByType(type);
	}
	
	// Returns a count of all events by the current user by the type
	public int getEventsCountByUserByType(long userId, String type) throws SystemException {
		return userEventPersistence.countByUserAndType(userId, type);
	}
	
}
	