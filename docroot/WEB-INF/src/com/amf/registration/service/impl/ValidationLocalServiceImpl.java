/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service.impl;

import com.amf.registration.service.base.ValidationLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.ListTypeConstants;
import com.liferay.portal.model.Region;
import com.liferay.portal.model.User;
import com.liferay.portal.service.AddressLocalServiceUtil;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.ListTypeServiceUtil;
import com.liferay.portal.service.PhoneLocalServiceUtil;
import com.liferay.portal.service.RegionServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.portlet.ActionRequest;

import java.util.regex.*;

public class ValidationLocalServiceImpl extends ValidationLocalServiceBaseImpl {

	int prefixId = 0;
	int suffixId = 0;
	String middleName = "";
	boolean primary = true;
	ServiceContext serviceContext = new ServiceContext();
	
	public void validateInput(ActionRequest actionRequest) {
		
		// Fetches all parameters from POST request
		String password1 = actionRequest.getParameter("password1");
		String password2 = actionRequest.getParameter("password2");
		String screenName = actionRequest.getParameter("username");
		String emailAddress = actionRequest.getParameter("email_address");
		String firstName = actionRequest.getParameter("first_name");
		String lastName = actionRequest.getParameter("last_name");
		boolean male = Boolean.valueOf(actionRequest.getParameter("male"));
		int birthdayMonth = Integer.parseInt(actionRequest.getParameter("b_month"));
		int birthdayDay = Integer.parseInt(actionRequest.getParameter("b_day"));
		int birthdayYear = Integer.parseInt(actionRequest.getParameter("b_year"));
		String question = actionRequest.getParameter("security_question");
		String answer = actionRequest.getParameter("security_answer");
		String toa = actionRequest.getParameter("accepted_tou");
		String street1 = actionRequest.getParameter("address");
		String street2 = actionRequest.getParameter("address2");
		String city = actionRequest.getParameter("city");
		String zip = actionRequest.getParameter("zip");
		String state = actionRequest.getParameter("state");
		String home_phone = actionRequest.getParameter("home_phone");
		String mobile_phone = actionRequest.getParameter("mobile_phone");
		
		boolean valid = true;
		
		// Runs validation on all pertinent fields
		if (!passwordCheck(password1, password2)) {
			valid = false;
		} else if (!charCheck(firstName, 50, true, false)) {
			valid = false;
		} else if (!charCheck(lastName, 50, true, false)) {
			valid = false;
		} else if (!emailCheck(emailAddress, 255)) {
			valid = false;
		} else if (!charCheck(street1, 255, false, true)) {
			valid = false;
		} else if (!charCheck(street2, 255, false, true)) {
			valid = false;
		} else if (!charCheck(city, 255, false, true)) {
			valid = false;
		} else if (!charCheck(answer, 255, true, false)) {
			valid = false;
		} else if (!birthdayCheck(birthdayMonth, birthdayDay, birthdayYear)) {
			valid = false;
		} else if (!charCheck(home_phone, 10, false, false)) {
			valid = false;
		} else if (!charCheck(mobile_phone, 10, false, false)) {
			valid = false;
		} else if (!charCheck(zip, 5, false, false)) {
			valid = false;
		}
		
		// If all validation is successful, begin user creation
		if (valid) {
			
			long[] identification = addUser(actionRequest, firstName, lastName, emailAddress, screenName, male, birthdayMonth-1, birthdayDay, birthdayYear, password1, password2, question, answer, toa);
			long userId = identification[0];
			long classPK = identification[1];
			String className = Contact.class.getName();
			
			// Adds address information into DB
			addAddress(street1, street2, city, zip, state, userId, className, classPK);
			// Adds phone information into DB
			addPhone(home_phone, mobile_phone, userId, className, classPK);
		}
	}
	
	// String validation
	public boolean charCheck(String word, int maxChar, boolean alphanum, boolean spaces) {
		
		// Check for if string requires alphanumeric validation
		if (alphanum) {
			Pattern alphanumeric = Pattern.compile("^[a-zA-Z0-9]*$");
			Matcher alphamatch = alphanumeric.matcher(word);
			if (!alphamatch.matches()) {
				return false;
			} else if (!(word.length() <= maxChar)) {
				return false;
			} else return true;
		// Check for if string contains spaces
		} else if (spaces) {
			if (word != "") {
				Pattern streets = Pattern.compile("^[A-Za-z0-9- ]+$");
				Matcher sm = streets.matcher(word);
				if (!sm.matches()) {
					return false;
				} else if (!(word.length() <= maxChar)) {
					return false;
				} else return true;
			} else return true;
		// Neither alphanumeric or space checking is required
		} else {
			if (!(word.length() == maxChar)) {
				return false;
			} else return true;
		} 
	}
	
	// Email validation
	public boolean emailCheck(String email, int maxChar) {
		
		Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(email);

		if (!(m.matches() && email.length() < maxChar)) {
			return false;
		} else return true;
	}
	
	// Password validation
	public boolean passwordCheck(String password1, String password2) {
		
		Pattern p = Pattern.compile("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$");
		Matcher m = p.matcher(password1);
		if (!(m.matches() && password1.equals(password2))) {
			return false;
		} else return true;
	}
	
	// Birthday validation
	public boolean birthdayCheck(int birthdayMonth, int birthdayDay, int birthdayYear) {
		
		String datePattern = "MM/dd/yyyy";
		SimpleDateFormat format = new SimpleDateFormat(datePattern);
		Date date;
		try {
			String dateString = Integer.valueOf(birthdayMonth).toString() + "/" + Integer.valueOf(birthdayDay).toString() + "/" + Integer.valueOf(birthdayYear).toString();
			date = format.parse(dateString);
			Calendar thirteenYears = Calendar.getInstance();
			thirteenYears.add(Calendar.YEAR, -13);
			Date thirteen = thirteenYears.getTime();
			if (date.compareTo(thirteen)<0) {
				return true;
			} else return false;
		}
		catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	// Add new user to user table in DB (and contacts)
	public long[] addUser(ActionRequest actionRequest, String firstName, String lastName, String emailAddress, String screenName, boolean male, int birthdayMonth, int birthdayDay, int birthdayYear, String password1, String password2, String question, String answer, String toa) {
			
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long creatorUserId = themeDisplay.getUserId();
			long companyId = themeDisplay.getCompanyId();
			Boolean autoPassword = false;
			Boolean autoScreenName = false;
			long facebookId = 0;
			String openId = "";
			Locale locale = themeDisplay.getLocale();
			String jobTitle = "";
			long[] groupIds = null;
			long[] organizationIds = null;
			long[] roleIds = null;
			long[] userGroupIds = null;
			Boolean sendEmail = false;
			
			boolean agreedToTermsOfUse;
			
			if (toa.equals("true")) {
				agreedToTermsOfUse = true;
			} else {
				agreedToTermsOfUse = false;
			}
			
			User user;
			long userId = 0;
			long contactId = 0;
			
			// Inserts into DB and updates row with additional info
			try {
				user = UserLocalServiceUtil.addUser(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, facebookId, openId, locale, firstName, middleName, lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, serviceContext);
				userId = user.getUserId();
				contactId = user.getContactId();
				UserLocalServiceUtil.updateReminderQuery(userId, question, answer);
				UserLocalServiceUtil.updateAgreedToTermsOfUse(userId, agreedToTermsOfUse);
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (PortalException e) {
				e.printStackTrace();
			}
			
			return new long[] {userId, contactId};
		}
	
	// Add new address pertaining to new user
	public void addAddress(String street1, String street2, String city, String zip, String state, long userId, String className, long classPK) {
		
		String street3 = "";
		long countryId = 19;
		try {
			Region region = RegionServiceUtil.getRegion(countryId, state);
			long regionId = region.getRegionId();
			int typeId = 11002;
			boolean mailing = false;
			AddressLocalServiceUtil.addAddress(userId, className, classPK, street1, street2, street3, city, zip, regionId, countryId, typeId, mailing, primary, serviceContext);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	// Add new phone pertaining to new user
	public void addPhone(String home_phone, String mobile_phone, long userId, String className, long classPK) {
		String extension = "";
		try {
			PhoneLocalServiceUtil.addPhone(userId, className, classPK, home_phone, extension, 11011, primary, serviceContext);
			PhoneLocalServiceUtil.addPhone(userId , className, classPK, mobile_phone, extension, 11008, primary, serviceContext);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

}