/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service.impl;

import com.amf.registration.UserEventPermission;
import com.amf.registration.model.UserEvent;
import com.amf.registration.service.UserEventLocalServiceUtil;
import com.amf.registration.service.base.UserEventServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.auth.PrincipalException;
import com.amf.registration.ActionKeys;

import java.util.List;

public class UserEventServiceImpl extends UserEventServiceBaseImpl {
	
	// Verifies proper user permissions to find all user events
	public List<UserEvent> getUserEvents(long userId, long groupId, int start, int end) throws SystemException, PrincipalException, PortalException {
		if (UserEventPermission.check(getPermissionChecker(), groupId, ActionKeys.VIEW_EVERYBODY)) {
			return UserEventLocalServiceUtil.getUserEvents(start, end);
		} else {
			return UserEventLocalServiceUtil.getUserEventsByUser(userId, start, end);
		}
	}
	
	// Verifies proper user permissions to find user events by type
	public List<UserEvent> findEventByType(long userId, long groupId, String type, int start, int end) throws SystemException, PrincipalException, PortalException {
		if (UserEventPermission.check(getPermissionChecker(), groupId, ActionKeys.VIEW_EVERYBODY)) {
			return UserEventLocalServiceUtil.findEventByType(type, start, end);
		} else {
			return UserEventLocalServiceUtil.getUserEventsByUserByType(userId, type, start, end);
		}
	}
	
	// Verifies proper user permissions to get a count of all the events
	public int getEventsCount(long userId, long groupId) {
		try {
			if (UserEventPermission.check(getPermissionChecker(), groupId, ActionKeys.VIEW_EVERYBODY)) {
				return UserEventLocalServiceUtil.getEventsCount();
			} else {
				return UserEventLocalServiceUtil.getEventsCountByUser(userId);
			}
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
			return 0;
		}
	}

	// Verifies proper user permissions to get a count of events by the type
	public int getEventsCountByType(long userId, long groupId, String type) throws SystemException, PrincipalException, PortalException {
		if (UserEventPermission.check(getPermissionChecker(), groupId, ActionKeys.VIEW_EVERYBODY)) {
			return UserEventLocalServiceUtil.getEventsCountByType(type);
		} else {
			return UserEventLocalServiceUtil.getEventsCountByUserByType(userId, type);
		}
	}
}