package com.amf.registration;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.permission.PermissionChecker;

public class UserEventPermission {
	
	public static final String RESOURCE_NAME = "amfeventmonitorportlet_WAR_registrationportlet";
	
	public static boolean check(PermissionChecker permissionChecker, long groupId, String actionId) throws PortalException, SystemException {
		
		if (!contains(permissionChecker, groupId, actionId)) {
			return false;
		} else {
			return true;
		}
	}
	
	// Checks to see if user has permission to view the resource
	public static boolean contains(PermissionChecker permissionChecker, long groupId, String actionId) {
		// Automatically fails permissions if user is a guest
		if (groupId == 0) {
			return false;
		}
		return permissionChecker.hasPermission(groupId, RESOURCE_NAME, groupId, actionId);
	}

}
