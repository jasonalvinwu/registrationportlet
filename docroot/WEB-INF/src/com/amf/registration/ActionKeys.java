package com.amf.registration;

public class ActionKeys {

	public static final String VIEW = "VIEW";

	public static final String CONFIGURATION = "CONFIGURATION";

	public static final String ADD_ENTRY = "ADD_ENTRY";

	public static final String DELETE = "DELETE";

	public static final String SEARCH = "VIEW";

	public static final String FILTER = "FILTER";

	public static final String VIEW_EVERYBODY = "VIEW_EVERYBODY";

}
