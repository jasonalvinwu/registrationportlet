package com.amf.registration;

import com.amf.registration.service.ValidationLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;


public class RegistrationPortlet extends MVCPortlet {
	
	public void init() {
		viewTemplate = getInitParameter("view-template");
	}
	
	public void doView(
			RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
			
			include(viewTemplate, renderRequest, renderResponse); 
		
	}
	
	protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
	
	// Runs a wrapper service that validates input and creates new database objects
	public void validateUser(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException {
		ValidationLocalServiceUtil.validateInput(actionRequest);
	}
	

}
