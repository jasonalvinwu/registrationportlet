/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseLocalService;
import com.liferay.portal.service.InvokableLocalService;

import java.util.regex.*;

/**
 * Provides the local service interface for Validation. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Liferay
 * @see ValidationLocalServiceUtil
 * @see com.amf.registration.service.base.ValidationLocalServiceBaseImpl
 * @see com.amf.registration.service.impl.ValidationLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface ValidationLocalService extends BaseLocalService,
	InvokableLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ValidationLocalServiceUtil} to access the validation local service. Add custom service methods to {@link com.amf.registration.service.impl.ValidationLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier();

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier);

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable;

	public void validateInput(javax.portlet.ActionRequest actionRequest);

	public boolean charCheck(java.lang.String word, int maxChar,
		boolean alphanum, boolean spaces);

	public boolean emailCheck(java.lang.String email, int maxChar);

	public boolean passwordCheck(java.lang.String password1,
		java.lang.String password2);

	public boolean birthdayCheck(int birthdayMonth, int birthdayDay,
		int birthdayYear);

	public long[] addUser(javax.portlet.ActionRequest actionRequest,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String emailAddress, java.lang.String screenName,
		boolean male, int birthdayMonth, int birthdayDay, int birthdayYear,
		java.lang.String password1, java.lang.String password2,
		java.lang.String question, java.lang.String answer, java.lang.String toa);

	public void addAddress(java.lang.String street1, java.lang.String street2,
		java.lang.String city, java.lang.String zip, java.lang.String state,
		long userId, java.lang.String className, long classPK);

	public void addPhone(java.lang.String home_phone,
		java.lang.String mobile_phone, long userId, java.lang.String className,
		long classPK);
}