/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service;

import com.liferay.portal.service.InvokableLocalService;

/**
 * @author Liferay
 * @generated
 */
public class ValidationLocalServiceClp implements ValidationLocalService {
	public ValidationLocalServiceClp(
		InvokableLocalService invokableLocalService) {
		_invokableLocalService = invokableLocalService;

		_methodName0 = "getBeanIdentifier";

		_methodParameterTypes0 = new String[] {  };

		_methodName1 = "setBeanIdentifier";

		_methodParameterTypes1 = new String[] { "java.lang.String" };

		_methodName3 = "validateInput";

		_methodParameterTypes3 = new String[] { "javax.portlet.ActionRequest" };

		_methodName4 = "charCheck";

		_methodParameterTypes4 = new String[] {
				"java.lang.String", "int", "boolean", "boolean"
			};

		_methodName5 = "emailCheck";

		_methodParameterTypes5 = new String[] { "java.lang.String", "int" };

		_methodName6 = "passwordCheck";

		_methodParameterTypes6 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName7 = "birthdayCheck";

		_methodParameterTypes7 = new String[] { "int", "int", "int" };

		_methodName8 = "addUser";

		_methodParameterTypes8 = new String[] {
				"javax.portlet.ActionRequest", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"boolean", "int", "int", "int", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String"
			};

		_methodName9 = "addAddress";

		_methodParameterTypes9 = new String[] {
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "long",
				"java.lang.String", "long"
			};

		_methodName10 = "addPhone";

		_methodParameterTypes10 = new String[] {
				"java.lang.String", "java.lang.String", "long",
				"java.lang.String", "long"
			};
	}

	@Override
	public java.lang.String getBeanIdentifier() {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName0,
					_methodParameterTypes0, new Object[] {  });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.lang.String)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		try {
			_invokableLocalService.invokeMethod(_methodName1,
				_methodParameterTypes1,
				new Object[] { ClpSerializer.translateInput(beanIdentifier) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		throw new UnsupportedOperationException();
	}

	@Override
	public void validateInput(javax.portlet.ActionRequest actionRequest) {
		try {
			_invokableLocalService.invokeMethod(_methodName3,
				_methodParameterTypes3,
				new Object[] { ClpSerializer.translateInput(actionRequest) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}
	}

	@Override
	public boolean charCheck(java.lang.String word, int maxChar,
		boolean alphanum, boolean spaces) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName4,
					_methodParameterTypes4,
					new Object[] {
						ClpSerializer.translateInput(word),
						
					maxChar,
						
					alphanum,
						
					spaces
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return ((Boolean)returnObj).booleanValue();
	}

	@Override
	public boolean emailCheck(java.lang.String email, int maxChar) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName5,
					_methodParameterTypes5,
					new Object[] { ClpSerializer.translateInput(email), maxChar });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return ((Boolean)returnObj).booleanValue();
	}

	@Override
	public boolean passwordCheck(java.lang.String password1,
		java.lang.String password2) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName6,
					_methodParameterTypes6,
					new Object[] {
						ClpSerializer.translateInput(password1),
						
					ClpSerializer.translateInput(password2)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return ((Boolean)returnObj).booleanValue();
	}

	@Override
	public boolean birthdayCheck(int birthdayMonth, int birthdayDay,
		int birthdayYear) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName7,
					_methodParameterTypes7,
					new Object[] { birthdayMonth, birthdayDay, birthdayYear });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return ((Boolean)returnObj).booleanValue();
	}

	@Override
	public long[] addUser(javax.portlet.ActionRequest actionRequest,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String emailAddress, java.lang.String screenName,
		boolean male, int birthdayMonth, int birthdayDay, int birthdayYear,
		java.lang.String password1, java.lang.String password2,
		java.lang.String question, java.lang.String answer, java.lang.String toa) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName8,
					_methodParameterTypes8,
					new Object[] {
						ClpSerializer.translateInput(actionRequest),
						
					ClpSerializer.translateInput(firstName),
						
					ClpSerializer.translateInput(lastName),
						
					ClpSerializer.translateInput(emailAddress),
						
					ClpSerializer.translateInput(screenName),
						
					male,
						
					birthdayMonth,
						
					birthdayDay,
						
					birthdayYear,
						
					ClpSerializer.translateInput(password1),
						
					ClpSerializer.translateInput(password2),
						
					ClpSerializer.translateInput(question),
						
					ClpSerializer.translateInput(answer),
						
					ClpSerializer.translateInput(toa)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (long[])ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public void addAddress(java.lang.String street1, java.lang.String street2,
		java.lang.String city, java.lang.String zip, java.lang.String state,
		long userId, java.lang.String className, long classPK) {
		try {
			_invokableLocalService.invokeMethod(_methodName9,
				_methodParameterTypes9,
				new Object[] {
					ClpSerializer.translateInput(street1),
					
				ClpSerializer.translateInput(street2),
					
				ClpSerializer.translateInput(city),
					
				ClpSerializer.translateInput(zip),
					
				ClpSerializer.translateInput(state),
					
				userId,
					
				ClpSerializer.translateInput(className),
					
				classPK
				});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}
	}

	@Override
	public void addPhone(java.lang.String home_phone,
		java.lang.String mobile_phone, long userId, java.lang.String className,
		long classPK) {
		try {
			_invokableLocalService.invokeMethod(_methodName10,
				_methodParameterTypes10,
				new Object[] {
					ClpSerializer.translateInput(home_phone),
					
				ClpSerializer.translateInput(mobile_phone),
					
				userId,
					
				ClpSerializer.translateInput(className),
					
				classPK
				});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}
	}

	private InvokableLocalService _invokableLocalService;
	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
}