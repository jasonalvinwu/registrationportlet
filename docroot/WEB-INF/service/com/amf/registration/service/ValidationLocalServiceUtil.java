/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Validation. This utility wraps
 * {@link com.amf.registration.service.impl.ValidationLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Liferay
 * @see ValidationLocalService
 * @see com.amf.registration.service.base.ValidationLocalServiceBaseImpl
 * @see com.amf.registration.service.impl.ValidationLocalServiceImpl
 * @generated
 */
public class ValidationLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.amf.registration.service.impl.ValidationLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void validateInput(javax.portlet.ActionRequest actionRequest) {
		getService().validateInput(actionRequest);
	}

	public static boolean charCheck(java.lang.String word, int maxChar,
		boolean alphanum, boolean spaces) {
		return getService().charCheck(word, maxChar, alphanum, spaces);
	}

	public static boolean emailCheck(java.lang.String email, int maxChar) {
		return getService().emailCheck(email, maxChar);
	}

	public static boolean passwordCheck(java.lang.String password1,
		java.lang.String password2) {
		return getService().passwordCheck(password1, password2);
	}

	public static boolean birthdayCheck(int birthdayMonth, int birthdayDay,
		int birthdayYear) {
		return getService()
				   .birthdayCheck(birthdayMonth, birthdayDay, birthdayYear);
	}

	public static long[] addUser(javax.portlet.ActionRequest actionRequest,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String emailAddress, java.lang.String screenName,
		boolean male, int birthdayMonth, int birthdayDay, int birthdayYear,
		java.lang.String password1, java.lang.String password2,
		java.lang.String question, java.lang.String answer, java.lang.String toa) {
		return getService()
				   .addUser(actionRequest, firstName, lastName, emailAddress,
			screenName, male, birthdayMonth, birthdayDay, birthdayYear,
			password1, password2, question, answer, toa);
	}

	public static void addAddress(java.lang.String street1,
		java.lang.String street2, java.lang.String city, java.lang.String zip,
		java.lang.String state, long userId, java.lang.String className,
		long classPK) {
		getService()
			.addAddress(street1, street2, city, zip, state, userId, className,
			classPK);
	}

	public static void addPhone(java.lang.String home_phone,
		java.lang.String mobile_phone, long userId, java.lang.String className,
		long classPK) {
		getService()
			.addPhone(home_phone, mobile_phone, userId, className, classPK);
	}

	public static void clearService() {
		_service = null;
	}

	public static ValidationLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					ValidationLocalService.class.getName());

			if (invokableLocalService instanceof ValidationLocalService) {
				_service = (ValidationLocalService)invokableLocalService;
			}
			else {
				_service = new ValidationLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(ValidationLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(ValidationLocalService service) {
	}

	private static ValidationLocalService _service;
}