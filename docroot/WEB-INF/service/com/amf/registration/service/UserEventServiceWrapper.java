/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserEventService}.
 *
 * @author Liferay
 * @see UserEventService
 * @generated
 */
public class UserEventServiceWrapper implements UserEventService,
	ServiceWrapper<UserEventService> {
	public UserEventServiceWrapper(UserEventService userEventService) {
		_userEventService = userEventService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _userEventService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_userEventService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _userEventService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public java.util.List<com.amf.registration.model.UserEvent> getUserEvents(
		long userId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			com.liferay.portal.security.auth.PrincipalException {
		return _userEventService.getUserEvents(userId, groupId, start, end);
	}

	@Override
	public java.util.List<com.amf.registration.model.UserEvent> findEventByType(
		long userId, long groupId, java.lang.String type, int start, int end)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			com.liferay.portal.security.auth.PrincipalException {
		return _userEventService.findEventByType(userId, groupId, type, start,
			end);
	}

	@Override
	public int getEventsCount(long userId, long groupId) {
		return _userEventService.getEventsCount(userId, groupId);
	}

	@Override
	public int getEventsCountByType(long userId, long groupId,
		java.lang.String type)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException,
			com.liferay.portal.security.auth.PrincipalException {
		return _userEventService.getEventsCountByType(userId, groupId, type);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public UserEventService getWrappedUserEventService() {
		return _userEventService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedUserEventService(UserEventService userEventService) {
		_userEventService = userEventService;
	}

	@Override
	public UserEventService getWrappedService() {
		return _userEventService;
	}

	@Override
	public void setWrappedService(UserEventService userEventService) {
		_userEventService = userEventService;
	}

	private UserEventService _userEventService;
}