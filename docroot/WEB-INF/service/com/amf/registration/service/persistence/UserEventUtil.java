/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service.persistence;

import com.amf.registration.model.UserEvent;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the user event service. This utility wraps {@link UserEventPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Liferay
 * @see UserEventPersistence
 * @see UserEventPersistenceImpl
 * @generated
 */
public class UserEventUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(UserEvent userEvent) {
		getPersistence().clearCache(userEvent);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UserEvent> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UserEvent> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UserEvent> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static UserEvent update(UserEvent userEvent)
		throws SystemException {
		return getPersistence().update(userEvent);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static UserEvent update(UserEvent userEvent,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(userEvent, serviceContext);
	}

	/**
	* Returns all the user events where type = &#63;.
	*
	* @param type the type
	* @return the matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findByType(
		java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByType(type);
	}

	/**
	* Returns a range of all the user events where type = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param type the type
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @return the range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findByType(
		java.lang.String type, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByType(type, start, end);
	}

	/**
	* Returns an ordered range of all the user events where type = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param type the type
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findByType(
		java.lang.String type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByType(type, start, end, orderByComparator);
	}

	/**
	* Returns the first user event in the ordered set where type = &#63;.
	*
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent findByType_First(
		java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByType_First(type, orderByComparator);
	}

	/**
	* Returns the first user event in the ordered set where type = &#63;.
	*
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent fetchByType_First(
		java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByType_First(type, orderByComparator);
	}

	/**
	* Returns the last user event in the ordered set where type = &#63;.
	*
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent findByType_Last(
		java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByType_Last(type, orderByComparator);
	}

	/**
	* Returns the last user event in the ordered set where type = &#63;.
	*
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent fetchByType_Last(
		java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByType_Last(type, orderByComparator);
	}

	/**
	* Returns the user events before and after the current user event in the ordered set where type = &#63;.
	*
	* @param index the primary key of the current user event
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user event
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent[] findByType_PrevAndNext(
		long index, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByType_PrevAndNext(index, type, orderByComparator);
	}

	/**
	* Removes all the user events where type = &#63; from the database.
	*
	* @param type the type
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByType(java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByType(type);
	}

	/**
	* Returns the number of user events where type = &#63;.
	*
	* @param type the type
	* @return the number of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static int countByType(java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByType(type);
	}

	/**
	* Returns all the user events where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findByUserID(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserID(userId);
	}

	/**
	* Returns a range of all the user events where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @return the range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findByUserID(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserID(userId, start, end);
	}

	/**
	* Returns an ordered range of all the user events where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findByUserID(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserID(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first user event in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent findByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserID_First(userId, orderByComparator);
	}

	/**
	* Returns the first user event in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent fetchByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserID_First(userId, orderByComparator);
	}

	/**
	* Returns the last user event in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent findByUserID_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserID_Last(userId, orderByComparator);
	}

	/**
	* Returns the last user event in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent fetchByUserID_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserID_Last(userId, orderByComparator);
	}

	/**
	* Returns the user events before and after the current user event in the ordered set where userId = &#63;.
	*
	* @param index the primary key of the current user event
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user event
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent[] findByUserID_PrevAndNext(
		long index, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserID_PrevAndNext(index, userId, orderByComparator);
	}

	/**
	* Removes all the user events where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserID(userId);
	}

	/**
	* Returns the number of user events where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserID(userId);
	}

	/**
	* Returns all the user events where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @return the matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findByUserAndType(
		long userId, java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserAndType(userId, type);
	}

	/**
	* Returns a range of all the user events where userId = &#63; and type = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param type the type
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @return the range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findByUserAndType(
		long userId, java.lang.String type, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserAndType(userId, type, start, end);
	}

	/**
	* Returns an ordered range of all the user events where userId = &#63; and type = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param type the type
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findByUserAndType(
		long userId, java.lang.String type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserAndType(userId, type, start, end,
			orderByComparator);
	}

	/**
	* Returns the first user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent findByUserAndType_First(
		long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserAndType_First(userId, type, orderByComparator);
	}

	/**
	* Returns the first user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent fetchByUserAndType_First(
		long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserAndType_First(userId, type, orderByComparator);
	}

	/**
	* Returns the last user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent findByUserAndType_Last(
		long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserAndType_Last(userId, type, orderByComparator);
	}

	/**
	* Returns the last user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent fetchByUserAndType_Last(
		long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUserAndType_Last(userId, type, orderByComparator);
	}

	/**
	* Returns the user events before and after the current user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param index the primary key of the current user event
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user event
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent[] findByUserAndType_PrevAndNext(
		long index, long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserAndType_PrevAndNext(index, userId, type,
			orderByComparator);
	}

	/**
	* Removes all the user events where userId = &#63; and type = &#63; from the database.
	*
	* @param userId the user ID
	* @param type the type
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserAndType(long userId, java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserAndType(userId, type);
	}

	/**
	* Returns the number of user events where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @return the number of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserAndType(long userId, java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserAndType(userId, type);
	}

	/**
	* Caches the user event in the entity cache if it is enabled.
	*
	* @param userEvent the user event
	*/
	public static void cacheResult(
		com.amf.registration.model.UserEvent userEvent) {
		getPersistence().cacheResult(userEvent);
	}

	/**
	* Caches the user events in the entity cache if it is enabled.
	*
	* @param userEvents the user events
	*/
	public static void cacheResult(
		java.util.List<com.amf.registration.model.UserEvent> userEvents) {
		getPersistence().cacheResult(userEvents);
	}

	/**
	* Creates a new user event with the primary key. Does not add the user event to the database.
	*
	* @param index the primary key for the new user event
	* @return the new user event
	*/
	public static com.amf.registration.model.UserEvent create(long index) {
		return getPersistence().create(index);
	}

	/**
	* Removes the user event with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param index the primary key of the user event
	* @return the user event that was removed
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent remove(long index)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(index);
	}

	public static com.amf.registration.model.UserEvent updateImpl(
		com.amf.registration.model.UserEvent userEvent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(userEvent);
	}

	/**
	* Returns the user event with the primary key or throws a {@link com.amf.registration.NoSuchUserEventException} if it could not be found.
	*
	* @param index the primary key of the user event
	* @return the user event
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent findByPrimaryKey(
		long index)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(index);
	}

	/**
	* Returns the user event with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param index the primary key of the user event
	* @return the user event, or <code>null</code> if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.amf.registration.model.UserEvent fetchByPrimaryKey(
		long index) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(index);
	}

	/**
	* Returns all the user events.
	*
	* @return the user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the user events.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @return the range of user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the user events.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user events
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.amf.registration.model.UserEvent> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the user events from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of user events.
	*
	* @return the number of user events
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static UserEventPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (UserEventPersistence)PortletBeanLocatorUtil.locate(com.amf.registration.service.ClpSerializer.getServletContextName(),
					UserEventPersistence.class.getName());

			ReferenceRegistry.registerReference(UserEventUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(UserEventPersistence persistence) {
	}

	private static UserEventPersistence _persistence;
}