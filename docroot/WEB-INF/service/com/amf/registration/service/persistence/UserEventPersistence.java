/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service.persistence;

import com.amf.registration.model.UserEvent;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the user event service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Liferay
 * @see UserEventPersistenceImpl
 * @see UserEventUtil
 * @generated
 */
public interface UserEventPersistence extends BasePersistence<UserEvent> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserEventUtil} to access the user event persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the user events where type = &#63;.
	*
	* @param type the type
	* @return the matching user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findByType(
		java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user events where type = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param type the type
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @return the range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findByType(
		java.lang.String type, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user events where type = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param type the type
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findByType(
		java.lang.String type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user event in the ordered set where type = &#63;.
	*
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent findByType_First(
		java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user event in the ordered set where type = &#63;.
	*
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent fetchByType_First(
		java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user event in the ordered set where type = &#63;.
	*
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent findByType_Last(
		java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user event in the ordered set where type = &#63;.
	*
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent fetchByType_Last(
		java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user events before and after the current user event in the ordered set where type = &#63;.
	*
	* @param index the primary key of the current user event
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user event
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent[] findByType_PrevAndNext(
		long index, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the user events where type = &#63; from the database.
	*
	* @param type the type
	* @throws SystemException if a system exception occurred
	*/
	public void removeByType(java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user events where type = &#63;.
	*
	* @param type the type
	* @return the number of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public int countByType(java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user events where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findByUserID(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user events where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @return the range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findByUserID(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user events where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findByUserID(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user event in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent findByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user event in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent fetchByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user event in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent findByUserID_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user event in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent fetchByUserID_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user events before and after the current user event in the ordered set where userId = &#63;.
	*
	* @param index the primary key of the current user event
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user event
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent[] findByUserID_PrevAndNext(
		long index, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the user events where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user events where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user events where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @return the matching user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findByUserAndType(
		long userId, java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user events where userId = &#63; and type = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param type the type
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @return the range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findByUserAndType(
		long userId, java.lang.String type, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user events where userId = &#63; and type = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param type the type
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findByUserAndType(
		long userId, java.lang.String type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent findByUserAndType_First(
		long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent fetchByUserAndType_First(
		long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event
	* @throws com.amf.registration.NoSuchUserEventException if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent findByUserAndType_Last(
		long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user event, or <code>null</code> if a matching user event could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent fetchByUserAndType_Last(
		long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user events before and after the current user event in the ordered set where userId = &#63; and type = &#63;.
	*
	* @param index the primary key of the current user event
	* @param userId the user ID
	* @param type the type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user event
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent[] findByUserAndType_PrevAndNext(
		long index, long userId, java.lang.String type,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the user events where userId = &#63; and type = &#63; from the database.
	*
	* @param userId the user ID
	* @param type the type
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserAndType(long userId, java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user events where userId = &#63; and type = &#63;.
	*
	* @param userId the user ID
	* @param type the type
	* @return the number of matching user events
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserAndType(long userId, java.lang.String type)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the user event in the entity cache if it is enabled.
	*
	* @param userEvent the user event
	*/
	public void cacheResult(com.amf.registration.model.UserEvent userEvent);

	/**
	* Caches the user events in the entity cache if it is enabled.
	*
	* @param userEvents the user events
	*/
	public void cacheResult(
		java.util.List<com.amf.registration.model.UserEvent> userEvents);

	/**
	* Creates a new user event with the primary key. Does not add the user event to the database.
	*
	* @param index the primary key for the new user event
	* @return the new user event
	*/
	public com.amf.registration.model.UserEvent create(long index);

	/**
	* Removes the user event with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param index the primary key of the user event
	* @return the user event that was removed
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent remove(long index)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.amf.registration.model.UserEvent updateImpl(
		com.amf.registration.model.UserEvent userEvent)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user event with the primary key or throws a {@link com.amf.registration.NoSuchUserEventException} if it could not be found.
	*
	* @param index the primary key of the user event
	* @return the user event
	* @throws com.amf.registration.NoSuchUserEventException if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent findByPrimaryKey(long index)
		throws com.amf.registration.NoSuchUserEventException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the user event with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param index the primary key of the user event
	* @return the user event, or <code>null</code> if a user event with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.amf.registration.model.UserEvent fetchByPrimaryKey(long index)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the user events.
	*
	* @return the user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the user events.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @return the range of user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the user events.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.amf.registration.model.impl.UserEventModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user events
	* @param end the upper bound of the range of user events (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user events
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.amf.registration.model.UserEvent> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the user events from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of user events.
	*
	* @return the number of user events
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}