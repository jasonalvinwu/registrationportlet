/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ValidationLocalService}.
 *
 * @author Liferay
 * @see ValidationLocalService
 * @generated
 */
public class ValidationLocalServiceWrapper implements ValidationLocalService,
	ServiceWrapper<ValidationLocalService> {
	public ValidationLocalServiceWrapper(
		ValidationLocalService validationLocalService) {
		_validationLocalService = validationLocalService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _validationLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_validationLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _validationLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public void validateInput(javax.portlet.ActionRequest actionRequest) {
		_validationLocalService.validateInput(actionRequest);
	}

	@Override
	public boolean charCheck(java.lang.String word, int maxChar,
		boolean alphanum, boolean spaces) {
		return _validationLocalService.charCheck(word, maxChar, alphanum, spaces);
	}

	@Override
	public boolean emailCheck(java.lang.String email, int maxChar) {
		return _validationLocalService.emailCheck(email, maxChar);
	}

	@Override
	public boolean passwordCheck(java.lang.String password1,
		java.lang.String password2) {
		return _validationLocalService.passwordCheck(password1, password2);
	}

	@Override
	public boolean birthdayCheck(int birthdayMonth, int birthdayDay,
		int birthdayYear) {
		return _validationLocalService.birthdayCheck(birthdayMonth,
			birthdayDay, birthdayYear);
	}

	@Override
	public long[] addUser(javax.portlet.ActionRequest actionRequest,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String emailAddress, java.lang.String screenName,
		boolean male, int birthdayMonth, int birthdayDay, int birthdayYear,
		java.lang.String password1, java.lang.String password2,
		java.lang.String question, java.lang.String answer, java.lang.String toa) {
		return _validationLocalService.addUser(actionRequest, firstName,
			lastName, emailAddress, screenName, male, birthdayMonth,
			birthdayDay, birthdayYear, password1, password2, question, answer,
			toa);
	}

	@Override
	public void addAddress(java.lang.String street1, java.lang.String street2,
		java.lang.String city, java.lang.String zip, java.lang.String state,
		long userId, java.lang.String className, long classPK) {
		_validationLocalService.addAddress(street1, street2, city, zip, state,
			userId, className, classPK);
	}

	@Override
	public void addPhone(java.lang.String home_phone,
		java.lang.String mobile_phone, long userId, java.lang.String className,
		long classPK) {
		_validationLocalService.addPhone(home_phone, mobile_phone, userId,
			className, classPK);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ValidationLocalService getWrappedValidationLocalService() {
		return _validationLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedValidationLocalService(
		ValidationLocalService validationLocalService) {
		_validationLocalService = validationLocalService;
	}

	@Override
	public ValidationLocalService getWrappedService() {
		return _validationLocalService;
	}

	@Override
	public void setWrappedService(ValidationLocalService validationLocalService) {
		_validationLocalService = validationLocalService;
	}

	private ValidationLocalService _validationLocalService;
}