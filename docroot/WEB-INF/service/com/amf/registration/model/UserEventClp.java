/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.model;

import com.amf.registration.service.ClpSerializer;
import com.amf.registration.service.UserEventLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Liferay
 */
public class UserEventClp extends BaseModelImpl<UserEvent> implements UserEvent {
	public UserEventClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UserEvent.class;
	}

	@Override
	public String getModelClassName() {
		return UserEvent.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _index;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIndex(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _index;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("index", getIndex());
		attributes.put("screenName", getScreenName());
		attributes.put("userId", getUserId());
		attributes.put("date", getDate());
		attributes.put("ipAddress", getIpAddress());
		attributes.put("type", getType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long index = (Long)attributes.get("index");

		if (index != null) {
			setIndex(index);
		}

		String screenName = (String)attributes.get("screenName");

		if (screenName != null) {
			setScreenName(screenName);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		String ipAddress = (String)attributes.get("ipAddress");

		if (ipAddress != null) {
			setIpAddress(ipAddress);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	@Override
	public long getIndex() {
		return _index;
	}

	@Override
	public void setIndex(long index) {
		_index = index;

		if (_userEventRemoteModel != null) {
			try {
				Class<?> clazz = _userEventRemoteModel.getClass();

				Method method = clazz.getMethod("setIndex", long.class);

				method.invoke(_userEventRemoteModel, index);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getScreenName() {
		return _screenName;
	}

	@Override
	public void setScreenName(String screenName) {
		_screenName = screenName;

		if (_userEventRemoteModel != null) {
			try {
				Class<?> clazz = _userEventRemoteModel.getClass();

				Method method = clazz.getMethod("setScreenName", String.class);

				method.invoke(_userEventRemoteModel, screenName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_userEventRemoteModel != null) {
			try {
				Class<?> clazz = _userEventRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_userEventRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public void setDate(Date date) {
		_date = date;

		if (_userEventRemoteModel != null) {
			try {
				Class<?> clazz = _userEventRemoteModel.getClass();

				Method method = clazz.getMethod("setDate", Date.class);

				method.invoke(_userEventRemoteModel, date);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIpAddress() {
		return _ipAddress;
	}

	@Override
	public void setIpAddress(String ipAddress) {
		_ipAddress = ipAddress;

		if (_userEventRemoteModel != null) {
			try {
				Class<?> clazz = _userEventRemoteModel.getClass();

				Method method = clazz.getMethod("setIpAddress", String.class);

				method.invoke(_userEventRemoteModel, ipAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getType() {
		return _type;
	}

	@Override
	public void setType(String type) {
		_type = type;

		if (_userEventRemoteModel != null) {
			try {
				Class<?> clazz = _userEventRemoteModel.getClass();

				Method method = clazz.getMethod("setType", String.class);

				method.invoke(_userEventRemoteModel, type);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUserEventRemoteModel() {
		return _userEventRemoteModel;
	}

	public void setUserEventRemoteModel(BaseModel<?> userEventRemoteModel) {
		_userEventRemoteModel = userEventRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _userEventRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_userEventRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UserEventLocalServiceUtil.addUserEvent(this);
		}
		else {
			UserEventLocalServiceUtil.updateUserEvent(this);
		}
	}

	@Override
	public UserEvent toEscapedModel() {
		return (UserEvent)ProxyUtil.newProxyInstance(UserEvent.class.getClassLoader(),
			new Class[] { UserEvent.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UserEventClp clone = new UserEventClp();

		clone.setIndex(getIndex());
		clone.setScreenName(getScreenName());
		clone.setUserId(getUserId());
		clone.setDate(getDate());
		clone.setIpAddress(getIpAddress());
		clone.setType(getType());

		return clone;
	}

	@Override
	public int compareTo(UserEvent userEvent) {
		int value = 0;

		value = DateUtil.compareTo(getDate(), userEvent.getDate());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserEventClp)) {
			return false;
		}

		UserEventClp userEvent = (UserEventClp)obj;

		long primaryKey = userEvent.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{index=");
		sb.append(getIndex());
		sb.append(", screenName=");
		sb.append(getScreenName());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", date=");
		sb.append(getDate());
		sb.append(", ipAddress=");
		sb.append(getIpAddress());
		sb.append(", type=");
		sb.append(getType());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("com.amf.registration.model.UserEvent");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>index</column-name><column-value><![CDATA[");
		sb.append(getIndex());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>screenName</column-name><column-value><![CDATA[");
		sb.append(getScreenName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ipAddress</column-name><column-value><![CDATA[");
		sb.append(getIpAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>type</column-name><column-value><![CDATA[");
		sb.append(getType());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _index;
	private String _screenName;
	private long _userId;
	private String _userUuid;
	private Date _date;
	private String _ipAddress;
	private String _type;
	private BaseModel<?> _userEventRemoteModel;
	private Class<?> _clpSerializerClass = com.amf.registration.service.ClpSerializer.class;
}