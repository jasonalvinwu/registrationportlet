/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the UserEvent service. Represents a row in the &quot;Validation_UserEvent&quot; database table, with each column mapped to a property of this class.
 *
 * @author Liferay
 * @see UserEventModel
 * @see com.amf.registration.model.impl.UserEventImpl
 * @see com.amf.registration.model.impl.UserEventModelImpl
 * @generated
 */
public interface UserEvent extends UserEventModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link com.amf.registration.model.impl.UserEventImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
}