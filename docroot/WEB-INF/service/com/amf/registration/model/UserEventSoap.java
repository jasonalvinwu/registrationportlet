/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.amf.registration.service.http.UserEventServiceSoap}.
 *
 * @author Liferay
 * @see com.amf.registration.service.http.UserEventServiceSoap
 * @generated
 */
public class UserEventSoap implements Serializable {
	public static UserEventSoap toSoapModel(UserEvent model) {
		UserEventSoap soapModel = new UserEventSoap();

		soapModel.setIndex(model.getIndex());
		soapModel.setScreenName(model.getScreenName());
		soapModel.setUserId(model.getUserId());
		soapModel.setDate(model.getDate());
		soapModel.setIpAddress(model.getIpAddress());
		soapModel.setType(model.getType());

		return soapModel;
	}

	public static UserEventSoap[] toSoapModels(UserEvent[] models) {
		UserEventSoap[] soapModels = new UserEventSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserEventSoap[][] toSoapModels(UserEvent[][] models) {
		UserEventSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserEventSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserEventSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserEventSoap[] toSoapModels(List<UserEvent> models) {
		List<UserEventSoap> soapModels = new ArrayList<UserEventSoap>(models.size());

		for (UserEvent model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserEventSoap[soapModels.size()]);
	}

	public UserEventSoap() {
	}

	public long getPrimaryKey() {
		return _index;
	}

	public void setPrimaryKey(long pk) {
		setIndex(pk);
	}

	public long getIndex() {
		return _index;
	}

	public void setIndex(long index) {
		_index = index;
	}

	public String getScreenName() {
		return _screenName;
	}

	public void setScreenName(String screenName) {
		_screenName = screenName;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public String getIpAddress() {
		return _ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		_ipAddress = ipAddress;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	private long _index;
	private String _screenName;
	private long _userId;
	private Date _date;
	private String _ipAddress;
	private String _type;
}