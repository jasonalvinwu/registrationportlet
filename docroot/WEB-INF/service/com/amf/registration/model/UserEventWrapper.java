/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.amf.registration.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UserEvent}.
 * </p>
 *
 * @author Liferay
 * @see UserEvent
 * @generated
 */
public class UserEventWrapper implements UserEvent, ModelWrapper<UserEvent> {
	public UserEventWrapper(UserEvent userEvent) {
		_userEvent = userEvent;
	}

	@Override
	public Class<?> getModelClass() {
		return UserEvent.class;
	}

	@Override
	public String getModelClassName() {
		return UserEvent.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("index", getIndex());
		attributes.put("screenName", getScreenName());
		attributes.put("userId", getUserId());
		attributes.put("date", getDate());
		attributes.put("ipAddress", getIpAddress());
		attributes.put("type", getType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long index = (Long)attributes.get("index");

		if (index != null) {
			setIndex(index);
		}

		String screenName = (String)attributes.get("screenName");

		if (screenName != null) {
			setScreenName(screenName);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		String ipAddress = (String)attributes.get("ipAddress");

		if (ipAddress != null) {
			setIpAddress(ipAddress);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	/**
	* Returns the primary key of this user event.
	*
	* @return the primary key of this user event
	*/
	@Override
	public long getPrimaryKey() {
		return _userEvent.getPrimaryKey();
	}

	/**
	* Sets the primary key of this user event.
	*
	* @param primaryKey the primary key of this user event
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_userEvent.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the index of this user event.
	*
	* @return the index of this user event
	*/
	@Override
	public long getIndex() {
		return _userEvent.getIndex();
	}

	/**
	* Sets the index of this user event.
	*
	* @param index the index of this user event
	*/
	@Override
	public void setIndex(long index) {
		_userEvent.setIndex(index);
	}

	/**
	* Returns the screen name of this user event.
	*
	* @return the screen name of this user event
	*/
	@Override
	public java.lang.String getScreenName() {
		return _userEvent.getScreenName();
	}

	/**
	* Sets the screen name of this user event.
	*
	* @param screenName the screen name of this user event
	*/
	@Override
	public void setScreenName(java.lang.String screenName) {
		_userEvent.setScreenName(screenName);
	}

	/**
	* Returns the user ID of this user event.
	*
	* @return the user ID of this user event
	*/
	@Override
	public long getUserId() {
		return _userEvent.getUserId();
	}

	/**
	* Sets the user ID of this user event.
	*
	* @param userId the user ID of this user event
	*/
	@Override
	public void setUserId(long userId) {
		_userEvent.setUserId(userId);
	}

	/**
	* Returns the user uuid of this user event.
	*
	* @return the user uuid of this user event
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userEvent.getUserUuid();
	}

	/**
	* Sets the user uuid of this user event.
	*
	* @param userUuid the user uuid of this user event
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_userEvent.setUserUuid(userUuid);
	}

	/**
	* Returns the date of this user event.
	*
	* @return the date of this user event
	*/
	@Override
	public java.util.Date getDate() {
		return _userEvent.getDate();
	}

	/**
	* Sets the date of this user event.
	*
	* @param date the date of this user event
	*/
	@Override
	public void setDate(java.util.Date date) {
		_userEvent.setDate(date);
	}

	/**
	* Returns the ip address of this user event.
	*
	* @return the ip address of this user event
	*/
	@Override
	public java.lang.String getIpAddress() {
		return _userEvent.getIpAddress();
	}

	/**
	* Sets the ip address of this user event.
	*
	* @param ipAddress the ip address of this user event
	*/
	@Override
	public void setIpAddress(java.lang.String ipAddress) {
		_userEvent.setIpAddress(ipAddress);
	}

	/**
	* Returns the type of this user event.
	*
	* @return the type of this user event
	*/
	@Override
	public java.lang.String getType() {
		return _userEvent.getType();
	}

	/**
	* Sets the type of this user event.
	*
	* @param type the type of this user event
	*/
	@Override
	public void setType(java.lang.String type) {
		_userEvent.setType(type);
	}

	@Override
	public boolean isNew() {
		return _userEvent.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_userEvent.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _userEvent.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userEvent.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _userEvent.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _userEvent.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_userEvent.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _userEvent.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_userEvent.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_userEvent.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_userEvent.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UserEventWrapper((UserEvent)_userEvent.clone());
	}

	@Override
	public int compareTo(com.amf.registration.model.UserEvent userEvent) {
		return _userEvent.compareTo(userEvent);
	}

	@Override
	public int hashCode() {
		return _userEvent.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.amf.registration.model.UserEvent> toCacheModel() {
		return _userEvent.toCacheModel();
	}

	@Override
	public com.amf.registration.model.UserEvent toEscapedModel() {
		return new UserEventWrapper(_userEvent.toEscapedModel());
	}

	@Override
	public com.amf.registration.model.UserEvent toUnescapedModel() {
		return new UserEventWrapper(_userEvent.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _userEvent.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userEvent.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_userEvent.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserEventWrapper)) {
			return false;
		}

		UserEventWrapper userEventWrapper = (UserEventWrapper)obj;

		if (Validator.equals(_userEvent, userEventWrapper._userEvent)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UserEvent getWrappedUserEvent() {
		return _userEvent;
	}

	@Override
	public UserEvent getWrappedModel() {
		return _userEvent;
	}

	@Override
	public void resetOriginalValues() {
		_userEvent.resetOriginalValues();
	}

	private UserEvent _userEvent;
}